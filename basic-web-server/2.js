/**
 * Basic Web Server 
 * 
 * Part 2
 */


// Require http
const http = require('http');

// Require fs
const fs = require('fs');




/**
 * This allows the ability to set the hostname. For example, here are some of example
 * hostnames
 * 
 * 1. localhost
 *              Localhost means it only runs on the local system only
 * 
 * 
 * 2. 127.0.0.1
 *              Localhost address only runs on the local system only
 * 
 * 
 * 3. <ip address in the network>
 *              This is where you have to specify the ip address in your network 
 *              to access to
 */

const hostname = '127.0.0.1'



/**
 * This allows the ability to set the ports. For example, here are some of example
 * ports
 * 
 * 1. 3000
 *             Description later...
 * 
 * 
 * 2. 4000
 *              Description later....
 * 
 * 
 * 3. 5000
 *              Description later....
 *              
 */

const port = 3000



// This is where we are going to establish a connection that will show the 
// contents of HTML

const server = http.createServer((req, res) => {
        if (req.url === '/') {
                fs.readFile('./Tests/index.html', (err, data) => {
                        if (err) throw err;
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'text/html');
                        res.end(data);
                });
        } else {
                res.statusCode = 404;
                res.setHeader('Content-Type', 'text/plain');
                res.end('404 Not Found\n');
        }
});





// This will output into the log indicating the server is running
// based on the const names that are defined. 



server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
});
