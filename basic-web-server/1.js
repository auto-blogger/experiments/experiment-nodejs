/**
 * Basic Web Server 
 * 
 * Part 1
 */

const http = require('http');                           // Get it to be required to use http

const hostname = '127.0.0.1'                            // This is where you define hostname.
                                                        // It can be either 127.0.0.1 or other ip address
                                                        // or just localhost

const port = 3000;                                      // This is where you define the port number. 


const server = http.createServer((req, res) => {
        res.statusCode = 200;   
        res.setHeader('Content-Type', 'text/plain');    // This is where you set the header type
        res.end('Hello World\n');                       // This is where you are able to add content
});
      
server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
});